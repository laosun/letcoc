Letcoc
======

(PHP) Library extends the capabilities of CodeIgniter.

RUS. Библиотека расширяющая возможности CodeIgniter.

======

##### Библиотека активно дописывается и переписывается!!!
##### То, что вы брали вчера, сегодня уже может устареть!

======

### ИНФОРМАЦИЯ

- Класс `_P` (или как я его окрестил "Плюшки") входящий в состав библиотеки,
  предназначен для получения информации об: переменных, константах, массивах,
  объектах, классах и подгружаемых файлах. По своей сути это набор методов,
  позволяющих минимизировать то, что мы делаем регулярно.


### SOME HELP

**Установка:** Скопировать файл `Letcoc.php` и папку `Letcoc_extends` в `./application/controllers/`

**Подключение в методе вашего контроллера:** `$this->load->library( "Letcoc", NULL, "Letcoc" );`

**Вызов справки по классу "_P":** `_P::DOC();`


### TODO

- По мере возможности составлю документацию (с примерами).
- Буду оттачивать визуальный вид выводимой справки по классам (_P::DOC();).
- Добавлю к выводимой информации, информацию о параметрах класса.
	
